import psycopg2
from datetime import datetime
import sys, jsontree

class Database:

    def __init__(self, config):

        self.host = config["host"]
        self.user = config["user"]
        self.db = config["database"]
        self.password = config["password"]
        self.port = config["port"]

    def connect(self):

        try:
            connection = psycopg2.connect(
                host = self.host,
                database = self.db,
                user = self.user,
                password = self.password,
                port = self.port
                )
            self.connection = connection
            print "Connected to database %s\n" % self.db

        except Exception, e:
            print "Cannot connect to database"

    def debug_InternalError(self):
        self.connection.close()
        self.connect()
        sys.stdout.write("Connection restarted !!\n")

    def create_tables(self):

        query = """
                CREATE TABLE IF NOT EXISTS data (
                    id bigserial primary key,
                    name varchar(100) NOT NULL,
                    label varchar(200) NOT NULL,
                    enr varchar(10) NOT NULL,
                    date_added timestamp default NULL
                );
        """

        connection = self.connection
        cursor = connection.cursor()

        try:
            cursor.execute(query)
            connection.commit()
            sys.stdout.write("Table has been created !!\n")

        except Exception, e:
            self.debug_InternalError()
            sys.stdout.write(str(e))

        cursor.close()

    #Adds Data to database, data is a jsontree object.
    def add(self, data):
        
        name = data.name
        label = data.label
        enr = data.enr
        date_added = datetime.now()

        status = jsontree.jsontree()

        query = """
                INSERT INTO data (
                    name, label, enr, date_added
                ) 
                VALUES (
                    \'%s\', \'%s\', \'%s\', \'%s\'
                );
        """ % (name, label, enr, date_added)

        connection = self.connection
        cursor = connection.cursor()

        try:
            cursor.execute(query)
            connection.commit()
            status.status = 1

        except Exception, e:
            status.status = 0
            status.msg = str(e)
            # raise e

        return status

    def get_all(self):

        query = """
            SELECT * FROM data;
        """

        cursor = self.connection.cursor()

        try:
            cursor.execute(query)
            result = cursor.fetchall()
            print result
            cursor.close()
            return result

        except Exception, e:
            self.debug_InternalError()
            sys.stdout.write(str(e))

            return {"status": 0}
        

import psycopg2
from datetime import datetime
import sys, jsontree
import requests as r

def connect():

    try:
        connection = psycopg2.connect(
            host = "localhost",
            database = "img",
            user = "nkman",
            password = "nonkman"
            )
        return connection

    except Exception, e:
        print "Cannot connect to database"

def get(connection):

	query = "SELECT * FROM data_container;"
	cursor = connection.cursor()

	cursor.execute(query)
	result = cursor.fetchall()

	count = 0
	for i in result:
		data = {
			"name": i[1],
			"label": i[2],
			"enr": i[3]
		}
		r.get("http://localhost:8080/add", params=data)
		print count
		count += 1


con = connect()
get(con)

from dmsin import app
from flask import Flask, jsonify, request, render_template
from db import db
import jsontree, json
import config

Config = config.config()
Db = db.Database(Config)
Db.connect()

@app.route('/', methods=['POST', 'GET'])
def home():
	return "Welcome here !!"

@app.route('/add', methods = ['GET'])
def add():

	data = jsontree.jsontree()
	data.name = request.args.get('name')
	data.label = data.name = request.args.get('label')
	data.enr = data.name = request.args.get('enr')
	# data.status = 0

	_data = Db.add(data)
	if (_data.status == 1):
		return "Good"
	else:
		return json.dumps(data)

@app.route('/get', methods=['POST', 'GET'])
def get():

	data = Db.get_all()
	print data
	return json.dumps(data)
